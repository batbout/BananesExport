# Bananes Export

BananesExport est une entreprise d'exportation de bananes imaginaire créer par AIOS SH pour évaluer diffrentes compétences, pratiques et maturités pour des développeurs Front ou Back ainsi que d'administration système/déploiement continu.

## Développement

Deux exercices sont disponibles pour la partie développement :
- Test Dev Back : [Back/README.md](Back/README.md)
- Test Dev Front : [Front/README.md](Front/README.md)

Le test dure entre 2 et 4h. Il est à réaliser d'où vous le souhaitez.

## Administration système et déploiement continu

L'exercice de test des déploiements et d'administration systèmes se trouve dans le répertoire [Deploy](Deploy/README.md). Il devrait également demander entre 2 et 4 heures et peut être réalisé d'où vous le souhaitez.

## Debrief

Nous réaliserons ensuite ensemble une revue de code dans nos locaux du [27 rue de Citeaux, 75012 PARIS](https://citymapper.com/directions?endaddress=75012+Paris%2C+France&endcoord=48.848788%2C2.380839&endname=27+Rue+de+C%C3%AEteaux&startcoord=48.861815%2C2.346831&startname=Ch%C3%A2telet+-+Les+Halles).

administration@aios.sh
